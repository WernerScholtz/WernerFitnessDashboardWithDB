package scholtz.werner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import scholtz.werner.domain.*;
import scholtz.werner.persistence.*;
import scholtz.werner.services.DeviceService;
import scholtz.werner.services.UserService;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {

//        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        SpringApplication.run(Application.class,args);


    }

}
