package scholtz.werner.commands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import scholtz.werner.domain.Users;
import scholtz.werner.services.UserService;

import javax.persistence.NoResultException;

@Component
public class IndexFormValidator implements Validator {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Users.class.equals(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {

        Users user = (Users) target;

        try {
            System.out.println("Checking if user exists");
            userService.getByUsername(user.getUsername());
            System.out.println("User exists");
        } catch (NoResultException e) {
            System.out.println("User doesn't exist");
            errors.rejectValue("username", "NoUsernameFound.username", "No such user name exists.");
        }

    }

}
