package scholtz.werner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import scholtz.werner.domain.Device;
import scholtz.werner.domain.Profile;
import scholtz.werner.domain.Users;
import scholtz.werner.services.DeviceService;
import scholtz.werner.services.ProfileService;
import scholtz.werner.services.UserService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {

    private ProfileService profileService;

    private UserService userService;

    private DeviceService deviceService;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    //----------------------------- Dashboard -----------------------------//
    // Logged in & display FD

    @RequestMapping("/loggedIn/{id}")
    public String loggedIn(@PathVariable Integer id, Model model) {
        Users user = userService.getById(id);
        model.addAttribute("user", user);
        return "/dashboard/fitnessDashboard";
    }

    //----------------------------- PROFILE -----------------------------//
    // Selecting profile

    @RequestMapping("/profile/{userID}")
    public String newProfile(@PathVariable Integer userID, Model model) {
        Users user = userService.getById(userID);
        Profile profile = new Profile();

        try {
            profile = profileService.getProfileByUserID(userID);
        } catch (NoResultException e) {
            profile.setUser(user);
        }

        model.addAttribute("profile", profile);
        model.addAttribute("user", user);
        return "/dashboard/profileform";
    }

    // Update profile & return to FD

    @RequestMapping(value = "/updateProfile/", method = RequestMethod.POST)
    public String updateProfile(Profile profile) {
        Profile updatedProfile = profileService.update(profile);
        return "redirect:/dashboard/loggedIn/" + updatedProfile.getUser().getUserID();
    }

    //----------------------------- DEVICES -----------------------------//

    @RequestMapping("/devices/{userID}")
    public String devices(@PathVariable Integer userID, Model model) {
        Users user = userService.getById(userID);
        Profile profile = profileService.getProfileByUserID(user.getUserID());
        Device device = new Device();
        device.setProfile(profile);

        model.addAttribute("devices", deviceService.listUserDevices(profile.getProfileID()));
        model.addAttribute("device", device);
        model.addAttribute("user", user);

        return "/dashboard/deviceform";
    }

    // Device button-action
    // Add device

    @RequestMapping(value = "/addDevice/", method = RequestMethod.POST)
    public String newDevice(Device device){
        Device newDevice = deviceService.update(device);
        return "redirect:/dashboard/devices/" + newDevice.getProfile().getUser().getUserID();
    }

    // Edit device

    @RequestMapping(value = "/editDevice/", method = RequestMethod.POST)
    public String editDevice(){

        return "";
    }

    // Remove device

    @RequestMapping(value = "/removeDevice/{deviceID}")
    public String removeDevice(@PathVariable Integer deviceID){
        Integer userID = deviceService.getById(deviceID).getProfile().getUser().getUserID();
        deviceService.delete(deviceID);
        return "redirect:/dashboard/devices/" + userID;
    }


}
