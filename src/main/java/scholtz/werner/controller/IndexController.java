package scholtz.werner.controller;

import com.microsoft.sqlserver.jdbc.SQLServerException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import scholtz.werner.commands.IndexFormValidator;
import scholtz.werner.domain.Devicemodel;
import scholtz.werner.domain.Users;
import scholtz.werner.services.ActivityService;
import scholtz.werner.services.DeviceModelService;
import scholtz.werner.services.UserService;

import javax.persistence.NoResultException;
import javax.validation.Valid;

@Controller
public class IndexController {

    private UserService userService;

    private DeviceModelService deviceModelService;

    private ActivityService activityService;

    private Validator indexFormValidator;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setDeviceModelService(DeviceModelService deviceModelService) {
        this.deviceModelService = deviceModelService;
    }

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    @Autowired
    @Qualifier("indexFormValidator")
    public void setIndexFormValidator(IndexFormValidator indexFormValidator) {
        this.indexFormValidator = indexFormValidator;
    }

    //----------------------------- index -----------------------------//

    @RequestMapping({"/",""})
    public String index(Model model) {
        model.addAttribute("user", new Users());
        return "index";
    }

    //----------------------------- index for unsuccessful login -----------------------------//

    @RequestMapping("/indexNoLogin")
    public String indexNoLogin(Model model) {
        model.addAttribute("user", new Users());
        return "indexNoLogin";
    }

    //----------------------------- User register -----------------------------//

    @RequestMapping(value = "/userRegisterLogin/", method = RequestMethod.POST, params = "action=register")
    public String register(Users user){

        Users registeringUser;

        try {
            registeringUser = userService.update(user);
        } catch (Exception e) { // Cannot catch SQLServerException. Thus just catch whatever exception is thrown :| ?wat?
            return "redirect:/indexNoLogin";
        }

        return "redirect:/dashboard/loggedIn/" + registeringUser.getUserID(); // Redirect to dashboard
    }

    //----------------------------- User login -----------------------------//

    @RequestMapping(value = "/userRegisterLogin/", method = RequestMethod.POST, params = "action=login")
    public String login(@Valid Users user, BindingResult bindingResult, Model model){

        indexFormValidator.validate(user, bindingResult);
//        System.out.println(bindingResult.getAllErrors());

        if (bindingResult.hasErrors()){
            System.out.println("User doesn't exist redirect");
            return "redirect:/";
        }

        Users registeredUser = userService.getByUsername(user.getUsername());
        checkPassword(registeredUser,user,bindingResult);

        if (bindingResult.hasErrors()){
            System.out.println("Wrong password");
            return "redirect:/";
        }

//        Users registeredUser;
//        try {
//            registeredUser = userService.getByUsername(user.getUsername());
//            checkPassword(registeredUser,user,bindingResult);
//        } catch (NoResultException e) {
//            bindingResult.rejectValue("username","NoUsernameFound.username","No such user name exists.");
//            return "redirect:/";
//        }


//        try {
//            registeredUser = userService.getByUsername(user.getUsername());
//            if (!checkPasswordOld(registeredUser,user)){
//                return "redirect:/indexNoLogin";
//            }
//        } catch (NoResultException e) {
//            return "redirect:/indexNoLogin";
//        }

        if (registeredUser.getRole().equals("admin")) {
            return "redirect:/admin/loggedIn/" + registeredUser.getUserID();
        }

        return "redirect:/dashboard/loggedIn/" + registeredUser.getUserID(); // Redirect to dashboard
    }


    //----------------------------- View supported devices & activities -----------------------------//

    @RequestMapping("/data/viewdevices")
    public String viewDevices(Model model){
        model.addAttribute("devicemodels", deviceModelService.listAll());
        return "data/deviceslist";
    }

    @RequestMapping("/data/viewactivities")
    public String viewActivities(Model model){
        model.addAttribute("activities", activityService.listAll());
        return "data/activitieslist";
    }

    //----------------------------- SUPPORTING METHODS -----------------------------//

    private void checkPassword(Users registeredUser, Users user, Errors errors) {
        if (!registeredUser.getPassword().equals(user.getPassword())) {
            errors.rejectValue("password","IncorrectPassword.password","Incorrect Username or Password");
        }
    }



}
