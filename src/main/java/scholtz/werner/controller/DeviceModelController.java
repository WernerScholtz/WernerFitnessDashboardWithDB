package scholtz.werner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import scholtz.werner.domain.Devicemodel;
import scholtz.werner.services.DeviceModelService;

@Controller
@RequestMapping("/deviceModel")
public class DeviceModelController {

    private DeviceModelService deviceModelService;

    @Autowired
    public void setDeviceModelService(DeviceModelService deviceModelService) {
        this.deviceModelService = deviceModelService;
    }


    //******************** Add new device model ********************//

    @RequestMapping(value = "/addDeviceModel/", method = RequestMethod.POST)
    public String newDeviceModel(Devicemodel devicemodel) {
        deviceModelService.update(devicemodel);
        return "redirect:/admin/viewDeviceModels";
    }

    //******************** Edit existing device model ********************//

    @RequestMapping("/edit/{devicemodelid}")
    public String edit(@PathVariable Integer devicemodelid, Model model){
        model.addAttribute("devicemodel",deviceModelService.getById(devicemodelid));
        return "/deviceModel/deviceModelForm";
    }

    @RequestMapping(value = "/deviceModelEdit/", method = RequestMethod.POST)
    public String update(Devicemodel devicemodel) {
        Devicemodel updatedDeviceModel = deviceModelService.update(devicemodel);
        return "redirect:/admin/viewDeviceModels";
    }

    //******************** Delete existing device model ********************//

    @RequestMapping("/delete/{devicemodelid}")
    public String delete(@PathVariable Integer devicemodelid) {
        deviceModelService.delete(devicemodelid);
        return "redirect:/admin/viewDeviceModels";
    }

}
