package scholtz.werner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import scholtz.werner.domain.Activity;
import scholtz.werner.domain.Devicemodel;
import scholtz.werner.domain.Users;
import scholtz.werner.services.ActivityService;
import scholtz.werner.services.DeviceModelService;
import scholtz.werner.services.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private UserService userService;

    private DeviceModelService deviceModelService;

    private ActivityService activityService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setDeviceModelService(DeviceModelService deviceModelService) {
        this.deviceModelService = deviceModelService;
    }

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }



    @RequestMapping("/loggedIn/{adminID}") // adminID is the user ID of the user with an admin role
    public String loggedIn(@PathVariable Integer adminID, Model model) {
        Users user = userService.getById(adminID);
        model.addAttribute("admin", user);
        return "admin/adminDashboard";
    }

    @RequestMapping("/viewUsers/{adminID}")
    public String listUsers (@PathVariable Integer adminID, Model model){
        model.addAttribute("admin", userService.getById(adminID));
        model.addAttribute("users",userService.listAll());
        return "admin/usersList";
    }

    @RequestMapping("/viewDeviceModels/{adminID}")
    public String listDeviceModels (@PathVariable Integer adminID, Model model){
        model.addAttribute("admin", userService.getById(adminID));
        model.addAttribute("devicemodels", deviceModelService.listAll());
        model.addAttribute("devicemodel", new Devicemodel());
        return "admin/deviceModelsList";
    }

    @RequestMapping("/viewActivities/{adminID}")
    public String listActivities (@PathVariable Integer adminID, Model model){
        model.addAttribute("admin", userService.getById(adminID));
        model.addAttribute("activities", activityService.listAll());
        model.addAttribute("activity", new Activity());
        return "admin/activitiesList";
    }

}
