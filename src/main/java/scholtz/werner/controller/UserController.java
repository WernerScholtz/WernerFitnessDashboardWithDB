package scholtz.werner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import scholtz.werner.domain.Users;
import scholtz.werner.services.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    //******************** Edit existing user ********************//

    @RequestMapping("/edit/{id}")
    public String edit(@PathVariable Integer id, Model model) {
        model.addAttribute("user", userService.getById(id));
        return "/user/registerlogin/";
    }

    @RequestMapping(value = "/userEdit/", method = RequestMethod.POST)
    public String update(Users user) {
        Users updatedUser = userService.update(user);
        return "redirect:/users/list";
    }

    //******************** Delete existing user ********************//

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        userService.delete(id);
        return "redirect:/admin/viewUsers";
    }


}
