package scholtz.werner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import scholtz.werner.domain.Activity;
import scholtz.werner.services.ActivityService;

@Controller
@RequestMapping("/activity")
public class ActivityController {

    private ActivityService activityService;

    @Autowired
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }


    //******************** Add new device model ********************//

    @RequestMapping(value = "/addActivity/", method = RequestMethod.POST)
    public String newActivity(Activity activity) {
        activityService.update(activity);
        return "redirect:/admin/viewActivities";
    }

    //******************** Edit existing device model ********************//

    @RequestMapping("/edit/{activityID}")
    public String edit(@PathVariable Integer activityID, Model model){
        model.addAttribute("activity",activityService.getById(activityID));
        return "/activity/activityForm";
    }

    @RequestMapping(value = "/activityEdit/", method = RequestMethod.POST)
    public String update(Activity activity) {
        Activity updatedActivity = activityService.update(activity);
        return "redirect:/admin/viewActivities";
    }

    //******************** Delete existing device model ********************//

    @RequestMapping("/delete/{activityID}")
    public String delete(@PathVariable Integer activityID) {
        activityService.delete(activityID);
        return "redirect:/admin/viewActivities";
    }

}
