package scholtz.werner.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Activity")
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer activityID;

//    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "activity", orphanRemoval = true)
//    private List<DeviceData> deviceData = new ArrayList<>();

    private String activityname;


    public Integer getActivityID() {
        return activityID;
    }

    public void setActivityID(Integer activityID) {
        this.activityID = activityID;
    }

//    public List<DeviceData> getDeviceData() {
//        return deviceData;
//    }
//
//    public void setDeviceData(List<DeviceData> deviceData) {
//        this.deviceData = deviceData;
//    }

    public String getActivityname() {
        return activityname;
    }

    public void setActivityname(String activityname) {
        this.activityname = activityname;
    }



}
