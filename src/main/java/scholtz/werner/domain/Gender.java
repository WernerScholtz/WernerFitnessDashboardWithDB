package scholtz.werner.domain;

import javax.persistence.*;

@Entity
@Table(name = "Gender")
public class Gender {
//    private enum validGender{male, female, apacheattackhelicopter} // Incorporate enum with DB???

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer genderID;
    private String gender;

    public int getGenderID() {
        return genderID;
    }

    public void setGenderID(Integer genderID) {
        this.genderID = genderID;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
