package scholtz.werner.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "profileID")
    private Integer profileID;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "userID")
    private Users user;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "profile", orphanRemoval = true)
    private List<Device> devices = new ArrayList<>();

//    private Integer genderID = 4;
    private String gender = "other";

    private String fullname;
    private String surname;
    private String profilename;
    private String email;
    private Double height; // Use Double rather than double as Double allows null where double doesn't allow null.
    private Integer age; // Use Integer rather than int as Integer allows null where int doesn't allow null.

    public Profile(){
    }

//    public Profile(Integer genderID) {
////        this.userID = userID;
//        this.genderID = genderID;
//    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Integer getProfileID() {
        return profileID;
    }

    public void setProfileID(Integer profileID) {
        this.profileID = profileID;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

//    public int getGenderID() {
//        return genderID;
//    }
//
//    public void setGenderID(Integer genderID) {
//        this.genderID = genderID;
//    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getProfilename() {
        return profilename;
    }

    public void setProfilename(String profilename) {
        this.profilename = profilename;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getHeight() {
        try { // NullPointerException is thrown if null is assigned to an Integer or Double.
            return height;
        } catch (NullPointerException e) {
            return height = 0.0;
        }
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public int getAge() {

        try { // NullPointerException is thrown if null is assigned to an Integer or Double.
            return age;
        } catch (NullPointerException e) {
            return age = 0;
        }

//        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }


}
