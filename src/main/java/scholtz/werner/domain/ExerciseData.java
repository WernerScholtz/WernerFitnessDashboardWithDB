package scholtz.werner.domain;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "ExerciseData")
public class ExerciseData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ExerciseDataID;

    @OneToOne
    @JoinColumn(name = "dataID")
    private DeviceData deviceData;

    private Integer averageHeartRate;
    private Double distance;
    private Double speed;
    private Double ascent;
    private Timestamp startDateTime;
    private Timestamp endDateTime;


    public Integer getExerciseDataID() {
        return ExerciseDataID;
    }

    public void setExerciseDataID(Integer exerciseDataID) {
        ExerciseDataID = exerciseDataID;
    }

    public DeviceData getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(DeviceData deviceData) {
        this.deviceData = deviceData;
    }

    public Integer getAverageHeartRate() {
        return averageHeartRate;
    }

    public void setAverageHeartRate(Integer averageHeartRate) {
        this.averageHeartRate = averageHeartRate;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Double getAscent() {
        return ascent;
    }

    public void setAscent(Double ascent) {
        this.ascent = ascent;
    }

    public Timestamp getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Timestamp startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Timestamp getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Timestamp endDateTime) {
        this.endDateTime = endDateTime;
    }
}
