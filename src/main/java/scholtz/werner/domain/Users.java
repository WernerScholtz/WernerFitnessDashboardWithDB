package scholtz.werner.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Users") // Table named Users due to "User" being a reserved word in SQL SERVER
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userID")
    private Integer userID;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "user", orphanRemoval = true)
    private Profile profile;

//    @Column(name = "username")
    @NotEmpty
    @Size(min = 3, max = 50)
    private String username;
//    @Column(name = "password")
    @NotEmpty
    private String password;
//    @Column(name = "roleID")
//    private Integer roleID = 2;

    private String role = "user";

    public Users() {
    }

//    public Users(String username, String password, int roleID) {
//        this.username = username;
//        this.password = password;
//        this.roleID = roleID;
//    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    //    public Integer getRoleID() {
//        return roleID;
//    }
//
//    public void setRoleID(Integer roleID) {
//        this.roleID = roleID;
//    }

}
