package scholtz.werner.domain;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "HealthData")
public class HealthData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer healthDataID;

    @OneToOne
    @JoinColumn(name = "dataID")
    private DeviceData deviceData;

    private Double weight;
    private Double bodyFat;
    private Date date;

    public Integer getHealthDataID() {
        return healthDataID;
    }

    public void setHealthDataID(Integer healthDataID) {
        this.healthDataID = healthDataID;
    }

    public DeviceData getDeviceData() {
        return deviceData;
    }

    public void setDeviceData(DeviceData deviceData) {
        this.deviceData = deviceData;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public double getBodyFat() {
        return bodyFat;
    }

    public void setBodyFat(Double bodyFat) {
        this.bodyFat = bodyFat;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}
