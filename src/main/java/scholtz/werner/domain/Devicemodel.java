package scholtz.werner.domain;

import javax.persistence.*;

@Entity
@Table(name = "Devicemodel")
public class Devicemodel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer devicemodelid;

//    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "deviceModel")
//    private List<Device> devices = new ArrayList<>();

    private String model;

    private String weburl;



    public Integer getDevicemodelid() {
        return devicemodelid;
    }

    public void setDevicemodelid(Integer devicemodelid) {
        this.devicemodelid = devicemodelid;
    }

//    public List<Device> getDevices() {
//        return devices;
//    }
//
//    public void setDevices(List<Device> devices) {
//        this.devices = devices;
//    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }

}
