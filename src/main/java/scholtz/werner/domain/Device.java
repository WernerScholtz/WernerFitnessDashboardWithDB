package scholtz.werner.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Device")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer deviceID;

    @ManyToOne
    @JoinColumn(name = "profileID")
    private Profile profile;

//    @ManyToOne
//    @JoinColumn(name = "deviceModelID")
//    private Devicemodel deviceModel;

//    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "device", orphanRemoval = true)
//    private List<DeviceData> deviceData = new ArrayList<>();

    private String devicemodel;
    private String name;


    public Integer getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(Integer deviceID) {
        this.deviceID = deviceID;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getDevicemodel() {
        return devicemodel;
    }

    public void setDevicemodel(String devicemodel) {
        this.devicemodel = devicemodel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


//    public Devicemodel getDeviceModel() {
//        return deviceModel;
//    }
//
//    public void setDeviceModel(Devicemodel deviceModel) {
//        this.deviceModel = deviceModel;
//    }

//    public List<DeviceData> getDeviceData() {
//        return deviceData;
//    }
//
//    public void setDeviceData(List<DeviceData> deviceData) {
//        this.deviceData = deviceData;
//    }

}
