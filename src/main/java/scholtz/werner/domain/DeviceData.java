package scholtz.werner.domain;

import javax.persistence.*;

@Entity
@Table(name = "DeviceData")
public class DeviceData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer dataID;

    @ManyToOne
    @JoinColumn(name = "deviceID")
    private Device device;

    @ManyToOne
    @JoinColumn(name = "activityID")
    private Activity activity;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "deviceData", orphanRemoval = true)
    private ExerciseData exerciseData;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, mappedBy = "deviceData", orphanRemoval = true)
    private HealthData healthData;


    public Integer getDataID() {
        return dataID;
    }

    public void setDataID(Integer dataID) {
        this.dataID = dataID;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public ExerciseData getExerciseData() {
        return exerciseData;
    }

    public void setExerciseData(ExerciseData exerciseData) {
        this.exerciseData = exerciseData;
    }

    public HealthData getHealthData() {
        return healthData;
    }

    public void setHealthData(HealthData healthData) {
        this.healthData = healthData;
    }

}
