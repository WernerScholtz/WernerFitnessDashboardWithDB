package scholtz.werner.persistence;

import org.apache.catalina.User;
import org.springframework.stereotype.Service;
import scholtz.werner.domain.Users;
import scholtz.werner.services.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

@Service("userService")
public class UserServiceJpaDaoImpl extends AbstractJpaDaoService implements UserService {

    @Override
    public void persistObject(Users user) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    @Override
    public Users update(Users user) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Users savedUser = em.merge(user);
        em.getTransaction().commit();
        return savedUser;
    }

    @Override
    public Users getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Users.class, id);
    }

    @Override
    public List<?> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Users", Users.class).getResultList();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Users.class, id));
        em.getTransaction().commit();
    }

    @Override
    public Users getByUsername(String username) {
        EntityManager em = emf.createEntityManager();
        Integer userID = (Integer) em.createQuery("SELECT userID FROM Users WHERE username = :custName").setParameter("custName", username).getSingleResult();
        return em.find(Users.class, userID);
    }

}
