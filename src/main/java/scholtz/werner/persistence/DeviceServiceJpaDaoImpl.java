package scholtz.werner.persistence;

import org.springframework.stereotype.Service;
import scholtz.werner.domain.Device;
import scholtz.werner.services.DeviceService;

import javax.persistence.EntityManager;
import java.util.List;

@Service("deviceService")
public class DeviceServiceJpaDaoImpl extends AbstractJpaDaoService implements DeviceService {

    @Override
    public Device update(Device device) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Device savedDevice = em.merge(device);
        em.getTransaction().commit();
        return savedDevice;
    }

    @Override
    public List<Device> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Device",Device.class).getResultList();
    }

    @Override
    public Device getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Device.class,id);
    }

    @Override
    public void persistObject(Device device) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(device);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Device.class,id));
        em.getTransaction().commit();
    }

    @Override
    public List listUserDevices(Integer profileId) {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("FROM Device WHERE profileID = :profileId").setParameter("profileId",profileId).getResultList();
    }

}
