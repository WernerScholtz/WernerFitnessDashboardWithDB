package scholtz.werner.persistence;

import scholtz.werner.domain.DeviceData;
import scholtz.werner.services.DeviceDataService;

import javax.persistence.EntityManager;
import java.util.List;

public class DeviceDataServiceJpaDaoImpl extends AbstractJpaDaoService implements DeviceDataService {

    @Override
    public DeviceData update(DeviceData domainObject) {
        return null;
    }

    @Override
    public List<DeviceData> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from DeviceData",DeviceData.class).getResultList();
    }

    @Override
    public DeviceData getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(DeviceData.class,id);
    }

    @Override
    public void persistObject(DeviceData deviceData) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(deviceData);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(DeviceData.class,id));
        em.getTransaction().commit();
    }

    public List listUserDeviceData(Integer deviceId) {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("FROM DeviceData WHERE deviceID = :deviceId").setParameter("deviceId",deviceId).getResultList();
    }
}
