package scholtz.werner.persistence;

import org.springframework.stereotype.Service;
import scholtz.werner.domain.Profile;
import scholtz.werner.services.ProfileService;

import javax.persistence.EntityManager;
import java.util.List;

@Service("profileService")
public class ProfileServiceJpaDaoImpl extends AbstractJpaDaoService implements ProfileService{

    @Override
    public Profile update(Profile profile) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Profile savedProfile = em.merge(profile);
        em.getTransaction().commit();
        return savedProfile;
    }

    @Override
    public void persistObject(Profile profile) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(profile);
        em.getTransaction().commit();
    }

    @Override
    public Profile getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Profile.class,id);
    }

    @Override
    public List<Profile> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Profile", Profile.class).getResultList();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Profile.class,id));
        em.getTransaction().commit();
    }

    @Override
    public Profile getProfileByUserID (Integer userID) {
        EntityManager em = emf.createEntityManager();
        Integer profileID = (Integer) em.createQuery("SELECT profileID FROM Profile WHERE userID = :custUserID").setParameter("custUserID",userID).getSingleResult();
        return em.find(Profile.class,profileID);
    }

}
