package scholtz.werner.persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;


public class AbstractJpaDaoService {

//    protected EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistence-unit");
    protected EntityManagerFactory emf;

    @PersistenceUnit
    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

}
