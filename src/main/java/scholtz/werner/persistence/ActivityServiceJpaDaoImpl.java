package scholtz.werner.persistence;

import org.springframework.stereotype.Service;
import scholtz.werner.domain.Activity;
import scholtz.werner.domain.Users;
import scholtz.werner.services.ActivityService;

import javax.persistence.EntityManager;
import java.util.List;

@Service("activityService")
public class ActivityServiceJpaDaoImpl extends AbstractJpaDaoService implements ActivityService {

    @Override
    public Activity update(Activity activity) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Activity savedActivity = em.merge(activity);
        em.getTransaction().commit();
        return savedActivity;
    }

    @Override
    public List<Activity> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Activity",Activity.class).getResultList();
    }

    @Override
    public Activity getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Activity.class,id);
    }

    @Override
    public void persistObject(Activity activity) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(activity);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Activity.class,id));
        em.getTransaction().commit();
    }

}
