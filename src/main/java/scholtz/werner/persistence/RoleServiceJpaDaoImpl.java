package scholtz.werner.persistence;

import scholtz.werner.domain.Role;
import scholtz.werner.services.RoleService;

import javax.persistence.EntityManager;
import java.util.List;

public class RoleServiceJpaDaoImpl extends AbstractJpaDaoService implements RoleService {

    @Override
    public List<Role> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Role",Role.class).getResultList();
    }

    @Override
    public Role getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Role.class,id);
    }

    @Override
    public void persistObject(Role role) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(role);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Role.class,id));
        em.getTransaction().commit();
    }

    @Override
    public Role update(Role domainObject) {
        return null;
    }
}
