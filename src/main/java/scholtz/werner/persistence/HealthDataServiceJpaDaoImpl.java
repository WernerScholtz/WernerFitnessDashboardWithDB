package scholtz.werner.persistence;

import scholtz.werner.domain.HealthData;
import scholtz.werner.services.HealthDataService;

import javax.persistence.EntityManager;
import java.util.List;

public class HealthDataServiceJpaDaoImpl extends AbstractJpaDaoService implements HealthDataService {

    @Override
    public HealthData update(HealthData domainObject) {
        return null;
    }

    @Override
    public List<HealthData> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from HealthData",HealthData.class).getResultList();
    }

    @Override
    public HealthData getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(HealthData.class,id);
    }

    @Override
    public void persistObject(HealthData healthData) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(healthData);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(HealthData.class,id));
        em.getTransaction().commit();
    }

}
