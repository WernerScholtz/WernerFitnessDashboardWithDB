package scholtz.werner.persistence;

import org.springframework.stereotype.Service;
import scholtz.werner.domain.Devicemodel;
import scholtz.werner.services.DeviceModelService;

import javax.persistence.EntityManager;
import java.util.List;

@Service("deviceModelService")
public class DeviceModelServiceJpaDaoImpl extends AbstractJpaDaoService implements DeviceModelService {

    @Override
    public Devicemodel update(Devicemodel devicemodel) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Devicemodel savedDeviceModel = em.merge(devicemodel);
        em.getTransaction().commit();
        return savedDeviceModel;
    }

    @Override
    public List<Devicemodel> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Devicemodel",Devicemodel.class).getResultList();
    }

    @Override
    public Devicemodel getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Devicemodel.class,id);
    }

    @Override
    public void persistObject(Devicemodel devicemodel) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(devicemodel);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(Devicemodel.class,id));
        em.getTransaction().commit();
    }

    public List listUserDeviceModels(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("SELECT model FROM Devicemodel WHERE deviceModelID = :userDeviceModelIDs").setParameter("userDeviceModelIDs",id).getResultList();
    }

}
