package scholtz.werner.persistence;

import scholtz.werner.domain.Gender;
import scholtz.werner.services.GenderService;

import javax.persistence.EntityManager;
import java.util.List;

public class GenderServiceJpaDaoImpl extends AbstractJpaDaoService implements GenderService{ // Not convinced that "Gender" needs an implementation service. . .


    @Override
    public Gender update(Gender domainObject) {
        return null;
    }

    @Override
    public void persistObject(Gender gender) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(gender);
        em.getTransaction().commit();
    }

    @Override
    public Gender getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(Gender.class,id);
    }

    @Override
    public List<Gender> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from Gender",Gender.class).getResultList();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Gender.class,id));
        em.getTransaction().commit();
    }

}
