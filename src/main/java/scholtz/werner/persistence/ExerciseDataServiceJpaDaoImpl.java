package scholtz.werner.persistence;

import scholtz.werner.domain.ExerciseData;
import scholtz.werner.services.ExerciseDateService;

import javax.persistence.EntityManager;
import java.util.List;

public class ExerciseDataServiceJpaDaoImpl extends AbstractJpaDaoService implements ExerciseDateService {

    @Override
    public ExerciseData update(ExerciseData domainObject) {
        return null;
    }

    @Override
    public List<ExerciseData> listAll() {
        EntityManager em = emf.createEntityManager();
        return em.createQuery("from ExerciseData",ExerciseData.class).getResultList();
    }

    @Override
    public ExerciseData getById(Integer id) {
        EntityManager em = emf.createEntityManager();
        return em.find(ExerciseData.class,id);
    }

    @Override
    public void persistObject(ExerciseData exerciseData) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(exerciseData);
        em.getTransaction().commit();
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.find(ExerciseData.class,id));
        em.getTransaction().commit();
    }

}
