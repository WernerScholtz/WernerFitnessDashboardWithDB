package scholtz.werner.services;

import scholtz.werner.domain.ExerciseData;

public interface ExerciseDateService extends CRUDService<ExerciseData> {
}
