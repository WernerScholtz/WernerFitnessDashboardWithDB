package scholtz.werner.services;

import scholtz.werner.domain.Gender;

public interface GenderService extends CRUDService<Gender>{
}
