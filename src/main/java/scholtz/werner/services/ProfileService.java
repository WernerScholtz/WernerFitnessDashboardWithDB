package scholtz.werner.services;

import scholtz.werner.domain.Profile;

public interface ProfileService extends CRUDService<Profile>{

    Profile getProfileByUserID(Integer userID);

}
