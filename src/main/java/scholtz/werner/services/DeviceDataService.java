package scholtz.werner.services;

import scholtz.werner.domain.DeviceData;

public interface DeviceDataService extends CRUDService<DeviceData> {
}
