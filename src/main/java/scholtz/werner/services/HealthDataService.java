package scholtz.werner.services;

import scholtz.werner.domain.HealthData;

public interface HealthDataService extends CRUDService<HealthData> {
}
