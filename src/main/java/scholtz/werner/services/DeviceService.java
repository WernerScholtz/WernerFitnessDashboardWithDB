package scholtz.werner.services;

import scholtz.werner.domain.Device;

import java.util.List;

public interface DeviceService extends CRUDService<Device> {

    List listUserDevices(Integer profileID);

}
