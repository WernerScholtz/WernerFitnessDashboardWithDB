package scholtz.werner.services;

import scholtz.werner.domain.Role;

public interface RoleService extends CRUDService<Role> {
}
