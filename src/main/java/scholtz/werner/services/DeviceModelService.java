package scholtz.werner.services;

import scholtz.werner.domain.Devicemodel;

public interface DeviceModelService extends CRUDService<Devicemodel> {
}
