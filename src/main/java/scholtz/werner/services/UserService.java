package scholtz.werner.services;

import scholtz.werner.domain.Users;

public interface UserService extends CRUDService<Users>{

    Users getByUsername(String username);

}
