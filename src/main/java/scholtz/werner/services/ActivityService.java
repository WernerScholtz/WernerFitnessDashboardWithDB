package scholtz.werner.services;

import scholtz.werner.domain.Activity;

public interface ActivityService extends CRUDService<Activity> {
}
