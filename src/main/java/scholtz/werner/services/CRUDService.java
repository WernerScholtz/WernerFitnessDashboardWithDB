package scholtz.werner.services;

import scholtz.werner.domain.Users;

import java.util.List;

public interface CRUDService<T> {

    List<?> listAll();

    T getById(Integer id);

    void persistObject(T domainObject);

    void delete(Integer id);

    T update(T domainObject);

}
